class StudentsController < ApplicationController
  before_action :logged_in_user, only: [:index, :edit, :update, :destroy]
  before_action :correct_user,   only: [:edit, :update, :destroy]
  
  def show
    @student = Student.find(params[:id])
  end

  def new
    @student = Student.new
  end

  def create
    @student = current_user.students.build(student_params)
    if @student.save
      flash[:success] = "Student created!"
      redirect_to @student
    else
      render 'new'
    end
  end
  
  def edit
    @student = Student.find(params[:id])
  end
  
  def update
    @student = Student.find(params[:id])
    if @student.update_attributes(student_params)
      flash[:success] = "Student updated"
      redirect_to @student
    else
      render 'edit'
    end
  end
  
  def destroy
    @student = Student.find(params[:id])
    @user = @student.user
    @student.destroy
    flash[:success] = "Student deleted"
    redirect_to user_url(@user)
  end
  
  private

    def student_params
      params.require(:student).permit(:name, :gender, :student_class, :date_known, :employment)
    end
    
     # Confirms the correct user.
    def correct_user
      @student = Student.find(params[:id])
      @user = @student.user
      redirect_to(root_url) unless current_user?(@user)
    end
  
end