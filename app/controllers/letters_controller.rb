class LettersController < ApplicationController
  before_action :logged_in_user, only: [:index, :edit, :update, :destroy]
  before_action :correct_user,   only: [:edit, :update, :destroy]
  
  def show
    @student = Student.find(params[:student_id])
    @letter = Letter.find(params[:id])
  end

  def new
    @letter = Letter.new
  end

  def create
    @student = Student.find(params[:student_id])
    @letter = @student.letters.build(letter_params)
    if @letter.save
      flash[:success] = "Letter created!"
      redirect_to student_letter_path(@student, @letter)
    else
      render 'new'
    end
  end
  
  def edit
   @letter = Letter.find(params[:id])
  end
  
  def update
    @letter = Letter.find(params[:id])
    @student = Student.find(params[:student_id])
    if @letter.update_attributes(letter_params)
      flash[:success] = "Letter updated"
      redirect_to student_letter_path(@student, @letter)
    else
      render 'edit'
    end
  end
  
  def destroy
    @student = Student.find(params[:student_id])
    @letter = Letter.find(params[:id])
    @letter.destroy
    flash[:success] = "Letter deleted"
    redirect_to student_url(@student)
  end
  
  private

    def letter_params
      params.require(:letter).permit(:rating, :words => [])
    end
    
     # Confirms the correct user.
    def correct_user
      @student = Student.find(params[:student_id])
      @user = @student.user
      redirect_to(root_url) unless current_user?(@user)
    end
    
end
