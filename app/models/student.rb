class Student < ActiveRecord::Base
  belongs_to :user
  has_many :letters, dependent: :destroy
  default_scope -> { order(:name) }
  validates :user_id, presence: true
  validates :name, presence: true
  validates :gender, presence: true
end
