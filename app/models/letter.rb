class Letter < ActiveRecord::Base
  belongs_to :student
  validates :rating, presence: true
  serialize :words, Array
  serialize :sentences, Array

end
