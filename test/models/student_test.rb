require 'test_helper'

class StudentTest < ActiveSupport::TestCase
  def setup
    @user = users(:michael)
    # This code is not idiomatically correct.
    @student = @user.students.build(name:"Frodo", gender:1, date_known:DateTime.new(1368, 9, 22), student_class:"Adventuring", user_id: @user.id)
  end

  test "should be valid" do
    assert @student.valid?
  end

  test "user id should be present" do
    @student.user_id = nil
    assert_not @student.valid?
  end
  
  test "name should be present" do
    @student.name = "   "
    assert_not @student.valid?
  end
  
  test "gender should be present" do
    @student.gender = nil
    assert_not @student.valid?
  end
end
