require 'test_helper'

class StaticPagesControllerTest < ActionController::TestCase
  long_title = "Letterhead"
  test "should get home" do
    get :home
    assert_response :success
    assert_select "title", long_title
  end

  test "should get help" do
    get :help
    assert_response :success
      assert_select "title", "Help | " + long_title
  end
  
   test "should get about" do
    get :about
    assert_response :success
    assert_select "title", "About | " + long_title
  end
  
    test "should get contact" do
    get :contact
    assert_response :success
    assert_select "title", "Contact | " + long_title
  end

end
