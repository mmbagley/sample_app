class AddWordsToLetters < ActiveRecord::Migration
  def change
    add_column :letters, :words, :text, array:true, default: []
  end
end
