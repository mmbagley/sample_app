class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.text :name
      t.integer :gender
      t.datetime :date_known
      t.text :student_class
      t.text :employment
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
