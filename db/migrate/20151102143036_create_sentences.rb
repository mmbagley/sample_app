class CreateSentences < ActiveRecord::Migration
  def change
    create_table :sentences do |t|
      t.text :content
      t.boolean :used
      t.integer :rating
      t.boolean :adlib

      t.timestamps null: false
    end
    add_column :letters, :sentences, :text, array:true, default: []
  end
end
