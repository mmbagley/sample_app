class CreateWords < ActiveRecord::Migration
  def change
    create_table :words do |t|
      t.text :content
      t.boolean :used
      t.integer :rating
      t.text :sentence

      t.timestamps null: false
    end
  end
end
