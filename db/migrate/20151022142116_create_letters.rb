class CreateLetters < ActiveRecord::Migration
  def change
    create_table :letters do |t|
      t.text :content
      t.integer :rating
      t.references :student, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
