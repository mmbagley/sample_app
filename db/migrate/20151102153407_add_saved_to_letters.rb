class AddSavedToLetters < ActiveRecord::Migration
  def change
    add_column :letters, :saved, :boolean
  end
end
