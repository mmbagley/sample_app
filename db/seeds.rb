# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

#User.create!(name:  "Example User",
#            email: "example@railstutorial.org",
#            password:              "foobar",
#            password_confirmation: "foobar",
#            admin: true,
#            activated: true,
#            activated_at: Time.zone.now)

# 99.times do |n|
#   name  = Faker::Name.name
#   email = "example-#{n+1}@railstutorial.org"
#   password = "password"
#   User.create!(name:  name,
#               email: email,
#               password:              password,
#               password_confirmation: password,
#               activated: true,
#               activated_at: Time.zone.now)
               
#users = User.order(:created_at).take(6)
#5.times do
#  name = Faker::Name.name
#  gender = 1
#  users.each { |user| user.students.create!(name: name, gender: gender) }
#end



Word.create!(
  content: "responsible",
  used: false,
  rating: 1,
  sentence: "%capPronoun% handles responsibility well.")
  
Word.create!(
  content: "innovative",
  used: false,
  rating: 1,
  sentence: "One of %pos% greatest talents is in developing innovations.")
  
Word.create!(
  content: "creative",
  used: false,
  rating: 1,
  sentence: "%capPronoun% shows creativity in %pos% work.")
  
Word.create!(
  content: "cheerful",
  used: false,
  rating: 1,
  sentence: "%capPronoun% remains cheerful even when in a stressful environment.")
  
Word.create!(
  content: "punctual",
  used: false,
  rating: 1,
  sentence: "%name%'s punctuality can be relied upon.")
  
Word.create!(
  content: "competent",
  used: false,
  rating: 1,
  sentence: "%capPronoun% is a quick and competent learner.")
  
Word.create!(
  content: "mature",
  used: false,
  rating: 1,
  sentence: "Showing maturity in any environment, %name% works well with little supervision.")
  
Word.create!(
  content: "late to class/work",
  used: false,
  rating: 0,
  sentence: "%capPronoun% tends to be late in any situation that calls for punctuality.")
  
Word.create!(
  content: "immature",
  used: false,
  rating: 0,
  sentence: "Due to %name%'s immature nature, supervision is recommended in %pos% early work.")
  
Word.create!(
  content: "incompetent",
  used: false,
  rating: 0,
  sentence: "Slow on the uptake, %name% may take some time to grasp new concepts thoroughly.")
  
  
  
Sentence.create!(
  content: "From our collaboration I can conclude that %name% has a strong motivation for %pos% work. %capPronoun% is a focused and determined person.",
  used: false,
  rating: 8,
  adlib: false)
  
Sentence.create!(
  content: "I would like to say that it is pleasant to work with %name%, %pronoun% is a %word1% and %word2% person.",
  used: false,
  rating: 8,
  adlib: true)
  
Sentence.create!(
  content: "%capPos% extraordinary ability to analyze problems and outline necessary courses of action was invaluable.",
  used: false,
  rating: 7,
  adlib: false)
  
Sentence.create!(
  content: "%capPronoun% concentrates all %pos% energies on the task assigned and follows instructions to the tee.",
  used: false,
  rating: 7,
  adlib: false)
  
Sentence.create!(
  content: "In some situations, %name% does not behave professionally.",
  used: false,
  rating: 2,
  adlib: false)
  
Sentence.create!(
  content: "%capPronoun% is professional in every environment.",
  used: false,
  rating: 7,
  adlib: false)
  
Sentence.create!(
  content: "%capPronoun% has an amazing ability to face challenges head on.",
  used: false,
  rating: 7,
  adlib: false)
  
Sentence.create!(
  content: "%names%'s work is decent and up to standard.",
  used: false,
  rating: 5,
  adlib: false)
  
Sentence.create!(
  content: "%names% is %word1%, however %pronoun% is %word3% and %word4% at times.",
  used: false,
  rating: 4,
  adlib: true)